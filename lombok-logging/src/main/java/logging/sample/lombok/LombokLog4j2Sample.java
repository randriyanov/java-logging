package logging.sample.lombok;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class LombokLog4j2Sample {

    public static void main(String[] args) {
        log.info("Hi from {}", "Logging");
    }
}
