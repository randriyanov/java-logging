package logging.libs.elk.repository;

import logging.libs.elk.domain.ToDo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ToDoRepository extends JpaRepository<ToDo, Long> {

    void deleteByUniqueId(String todoId);

}
