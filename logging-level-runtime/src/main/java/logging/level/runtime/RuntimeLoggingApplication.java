package logging.level.runtime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class RuntimeLoggingApplication {

    public static void main(String[] args) {
        SpringApplication.run(RuntimeLoggingApplication.class, args);
    }
}
