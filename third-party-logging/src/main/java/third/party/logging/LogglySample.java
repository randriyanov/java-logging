package third.party.logging;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class LogglySample {
    private static final Logger logger = LogManager.getLogger(LogglySample.class);

    public static void main(String[] args) {
        logger.debug("Debug log message");
        logger.info("Info log message");
        logger.error("Error log message");
    }
}
