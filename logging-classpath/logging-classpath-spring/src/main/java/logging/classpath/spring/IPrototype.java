package logging.classpath.spring;

public interface IPrototype {

    String getDateTime();
}
