package logging.libs.flogger;

import com.google.common.flogger.FluentLogger;
import com.google.common.flogger.LazyArgs;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

public class FloggerLoggingExample {

    private static final FluentLogger logger = FluentLogger.forEnclosingClass();

    public static void main(String[] args) {
        logger.atWarning().log("warning");

        logger.atInfo().log("info");

        logger.at(Level.SEVERE)
                .atMostEvery(50, TimeUnit.SECONDS)
                .log("SEVERE", LazyArgs.lazy(() -> doSomeCostly()));
    }


    public static int doSomeCostly() {
        return 0;
    }
}
