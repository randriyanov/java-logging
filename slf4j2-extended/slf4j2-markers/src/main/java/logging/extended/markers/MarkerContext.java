package logging.extended.markers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import org.apache.logging.log4j.ThreadContext;

public class MarkerContext {
    final static Marker DB_ERROR = MarkerManager.getMarker("DATABASE_ERROR");

    private static final Logger logger = LogManager.getLogger(MarkerContext.class);

    public static void main(String[] args) {
        logger.error(DB_ERROR, "An exception occurred.");
    }
}
