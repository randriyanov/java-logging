gradle logging-classpath:logging-classpath-hibernate:dependencies

dependencies {
    compile "ch.qos.logback:logback-classic:1.1.3"
    compile "org.slf4j:log4j-over-slf4j:1.7.13"
}

configurations.all {
    exclude group: "org.slf4j", module: "slf4j-log4j12"
    exclude group: "log4j", module: "log4j"
}

org.jboss.logging.LoggerProviders

JBOSS -> log4j -> SLF4J -> JUL/JDK