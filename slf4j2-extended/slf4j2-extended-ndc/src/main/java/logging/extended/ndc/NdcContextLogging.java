package logging.extended.ndc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

import java.util.UUID;

public class NdcContextLogging {
    private static final Logger logger = LogManager.getLogger(NdcContextLogging.class);

    public static void main(String[] args) {
        String username = "admin";
        String sessionID = UUID.randomUUID().toString();

        ThreadContext.push(username);
        ThreadContext.push(sessionID);

        logger.info("Login successful");

        ThreadContext.pop();
        ThreadContext.remove(sessionID);

        logger.info("Login empty");
    }
}
