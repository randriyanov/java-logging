package slf4j2.extended.filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoggingRunner {

    public static void main(String[] args) {
        Logger logger = LogManager.getLogger("slf4j2.extended.filter");

        logger.fatal("Fatal");
        logger.error("Error");
        logger.debug("Debug");
        logger.info("Info");

        System.out.println("end it");
    }
}
