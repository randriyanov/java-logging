package logging.spring.aspect.execution;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@ComponentScan("logging.spring.aspect.execution")
@EnableAspectJAutoProxy
public class Application {

    public static void main(String[] args) throws Exception {
        AnnotationConfigApplicationContext config = new AnnotationConfigApplicationContext(Application.class);
        Service service = config.getBean(Service.class);
        service.serve();
        Thread.sleep(50000L);
    }
}
