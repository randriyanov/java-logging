package logging.sample.lombok;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LombokLogbackSample {
    public static void main(String[] args) {
       log.info("Hi from Logging");
    }
}
