Some of the components you depend on rely on a logging API other than Log4j2 or SLF4J. You may also assume that these components will not switch to Log4j2 or SLF4J in the immediate future. To deal with such circumstances, we bridging modules which redirect calls made to log4j, JCL and java.util.logging APIs to behave as if they were made to the SLF4J API or Log4j2 API instead.

[https://docs.spring.io/spring/docs/5.0.0.RC3/spring-framework-reference/overview.html]
LOGGING 