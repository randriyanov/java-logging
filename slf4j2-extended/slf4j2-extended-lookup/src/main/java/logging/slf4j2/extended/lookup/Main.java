package logging.slf4j2.extended.lookup;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    final static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        logger.trace("Hello for {}", Main.class);
    }
}
