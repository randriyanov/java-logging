import com.ComApp;
import com.level.ComLevelDevApp;
import net.level.NetLevelDevApp;
import net.NetApp;

public class Main {

    public static void main(String[] args) {
        new ComApp();
        new ComLevelDevApp();
        new NetApp();
        new NetLevelDevApp();
    }
}
