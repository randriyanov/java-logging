By default, appenders are cumulative: a logger will log to the appenders attached to itself (if any) 
as well as all the appenders attached to its ancestors. 
Thus, attaching the same appender to multiple loggers will cause logging output to be duplicated.

[http://logging.apache.org/log4j/2.x/manual/garbagefree.html]
[https://www.boraji.com/log4j-2-rollingfileappender-example]